<?php
class Memory { 
    private $memory = [];

    function __construct() {
        $this->set_memory();
    }
    
    function get_usage() {
        return $this->memory;
    }
    
    function get_overview() {
        return [
            "total" => $this->memory["memory"]["total"] + $this->memory["swap"]["total"],
            "used" => $this->memory["memory"]["used"] + $this->memory["memory"]["shared"] + $this->memory["swap"]["used"] + $this->memory["swap"]["shared"]
        ];
    }
    
    function get_info() {
        return [
            "ram" => [
                "total" => $this->memory["memory"]["total"],
                "used" => $this->memory["memory"]["used"] + $this->memory["memory"]["shared"]
            ],
            "swap" => [
                "total" => $this->memory["swap"]["total"],
                "used" => $this->memory["swap"]["used"] + $this->memory["swap"]["shared"]
            ]
        ];
    }
    
    function set_memory() {
        $free = explode("\n", shell_exec("free"));

        $headings = preg_split("/[\s ]+/", trim($free[0]));
        $memories = preg_split("/[\s ]+/", trim($free[1]));
        $swaps = preg_split("/[\s ]+/", trim($free[2]));
    
        $memory = [];
        for ($i = 0; $i < count($headings); $i++) {
            $memory["memory"][$headings[$i]] = (!empty($memories[$i + 1]))? intval($memories[$i + 1]): 0;
        }
        for ($i = 0; $i < count($headings); $i++) {
            $memory["swap"][$headings[$i]] = (!empty($swaps[$i + 1]))? intval($swaps[$i + 1]): 0;
        }
    
        $this->memory = $memory;
    }
}