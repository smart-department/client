<?php
class Cpu { 
    
    function get_overview() {
        $cpu_commands = explode("\n", shell_exec("lscpu"));

        $overview = [];
        foreach ($cpu_commands as $cpu_command) {
            if (preg_match("/^CPU\(s\):/", $cpu_command) === 1) {
                $overview["threads"] = intval(preg_split("/[\s ]+/", trim($cpu_command))[1]);
            } else if (strpos($cpu_command, "Model name:") !== false) {
                $overview["model"] = trim(explode("Model name:", $cpu_command)[1]);
            } else if (strpos($cpu_command, "Vendor ID:") !== false) {
                $overview["vendor"] = trim(explode("Vendor ID:", $cpu_command)[1]);
            } else if (strpos($cpu_command, "CPU MHz:") !== false) {
                $overview["clock_rate"] = floatval(trim(explode("CPU MHz:", $cpu_command)[1]));
            } else if (strpos($cpu_command, "CPU max MHz:") !== false) { 
                $overview["clock_rate_max"] = floatval(trim(explode("CPU max MHz:", $cpu_command)[1]));
            } else if (strpos($cpu_command, "CPU min MHz:") !== false) { 
                $overview["clock_rate_min"] = floatval(trim(explode("CPU min MHz:", $cpu_command)[1]));
            }
        }

        return $overview;
    }

    function get_info() {
        $cpus = explode("power management:", shell_exec("cat /proc/cpuinfo"));

        $cpus_data = [];
        foreach($cpus as $cpu) {
            if (trim($cpu) === "") continue;
            preg_match("/processor\t: [0-9]+/", $cpu, $mat);
            $cpu_id = intval(trim(explode(": ", $mat[0])[1]));
            
            preg_match("/cpu MHz.*: [0-9.]+/", $cpu, $mat);
            $cpu_clock = floatval(trim(explode(": ", $mat[0])[1]));
            
            preg_match("/model\t+: [0-9]+/", $cpu, $mat);
            $cpu_model = intval((trim(explode(": ", $mat[0])[1])));
            
            preg_match("/cpu cores\t+: [0-9]+/", $cpu, $mat);
            $cpu_cores = intval((trim(explode(": ", $mat[0])[1])));
            
            preg_match("/model name\t+: .*+/", $cpu, $mat);
            $cpu_model_name = trim(explode(": ", $mat[0])[1]);
            
            preg_match("/vendor_id\t+: .*+/", $cpu, $mat);
            $cpu_vendor = trim(explode(": ", $mat[0])[1]);

            $overview = $this->get_overview();
            
            $cpu_data = null;
            for ($i = 0; $i < count($cpus_data); $i++) {
                if ($cpus_data[$i]["model_id"] === $cpu_model) {
                    $cpu_data = $i;
                    break;
                }
            }
            if (is_null($cpu_data)) {
                array_push($cpus_data, [
                        "model_id" => $cpu_model,
                        "vendor" => $cpu_vendor,
                        "model" => $cpu_model_name,
                        "threads" => 0,
                        "cores" => $cpu_cores,
                    ]);
                $cpu_data = count($cpus_data) - 1;
            }
            $cpus_data[$cpu_data]["threads"]++;
            $cpus_data[$cpu_data]["clock_rate"][$cpu_id] = $cpu_clock;
        }

        for ($i = 0; $i < count($cpus_data); $i++) {
            $cpus_data[$i]["clock_rate_average"] = array_sum($cpus_data[$i]["clock_rate"]) / count($cpus_data[$i]["clock_rate"]);
        }
            
        if (!empty($overview["clock_rate_max"])) {
            $cpus_data[0]["clock_rate_max"] = $overview["clock_rate_max"];
        }
        if (!empty($overview["clock_rate_min"])) {
            $cpus_data[0]["clock_rate_min"] = $overview["clock_rate_min"];
        }

        return $cpus_data;
    }
}