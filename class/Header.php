<?php

class Header {  
    static function get($property) {
        $headers = apache_request_headers();
        if (!empty($headers[$property])) {
            return $headers[$property];
        }
        return null;
    }

    static function get_token() {
        return @explode("Bearer ", self::get("Authorization"))[1];
    }
}