<?php
class System {
    function __construct($memory, $cpu) {
        $this->memory = $memory;
        $this->cpu = $cpu;
    }
    
    function get_overview() {
        $this->load_average = sys_getloadavg();

        return [
            "memory" => $this->memory->get_overview(),
            "load_average" => $this->load_average
        ];
    }

    function get_info() {
        $this->load_average = sys_getloadavg();
        $this->uptime = trim(shell_exec("uptime -p"), " up\n");
        $temp = array_map(function($el) { return ($el !== "")? explode("\t", $el)[1]: ""; }, explode("\n", shell_exec("lsb_release -a 2>/dev/null")));
        $this->variant = "{$temp[1]} ({$temp[3]})";

        $this->voltage = max(intval(shell_exec("cat /sys/class/power_supply/BAT*/voltage_now 2>/dev/null")) / 1000000, 0);
        $this->current = max(intval(shell_exec("cat /sys/class/power_supply/BAT*/current_now 2>/dev/null")) / 1000000, 0);
        
        $this->power_usage = [
            "voltage" => $this->voltage,
            "current" => $this->current,
            "power" => $this->voltage * $this->current
        ];

        $this->ip = trim(shell_exec("ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"), " \n");

        $meta = [
            "os" => php_uname("s"),
            "variant" => $this->variant,
            "host" => php_uname("n"),
            "release" => php_uname("r"),
            "machine_type" => php_uname("m"),
            "ip" => $this->ip
        ];
        

        return [
            "meta" => $meta,
            "load_average" => $this->load_average,
            "power_usage" => $this->power_usage,
            "uptime" => $this->uptime,
            "disk" => $this->get_disk_info(),
            "memory" => $this->memory->get_info(),
            "cpu" => $this->cpu->get_info()
        ];
    }

    function get_disk_info() {
        $usages = array_map(function($el) { return preg_split("/[\s ]+/", trim($el)); }, explode("\n", trim(shell_exec("df -x tmpfs"), "\n")));
        array_splice($usages, 0, 1);
        $disk = [];
        foreach ($usages as $usage) {
            $current_disk = [];
            $current_disk["mounted"] = $usage[5];
            $current_disk["filesystem"] = $usage[0];
            $current_disk["used"] = intval($usage[2]);
            $current_disk["available"] = intval($usage[3]);
            if ($current_disk["available"] > 0) {
                array_push($disk, $current_disk);
            }
        }
        return $disk;
    }

    function shutdown() {
        shell_exec("shutdown now");
    }
}