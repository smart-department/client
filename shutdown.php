<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/class/autoloader.php");

$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";

if ($method === "GET") {
    $KEYS = new Keys();
    
    if (!$KEYS->validate_auth(Header::get_token())) {
        Response::send(null, 401);
    }

    $memory = new Memory();
    $cpu = new Cpu();
    $system = new System($memory, $cpu);
    
    $system->shutdown();

    Response::send(null, 200, "Shutdown inititiated");
} else {
    Response::not_found();
}