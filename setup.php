<?php
require_once (__DIR__ . "/class/autoloader.php");

$KEYS = new Keys();

if (!Console::is_cli()) {
    Console::print ("only use in command line");
    Console::end();
}

$available_modes = ["config"];
$usage = "Usage: \nphp {$argv[0]} mode\n\nAVAILABLE MODES - " . implode(", ", $available_modes) . "\n";

if ($argc === 2 && $argv[1] === "config") {
    // init config
    Console::print ("This script will create a .env file in your current directory\n");
    if (file_exists(".env")) {
        Console::warn("A configuration file already exists.");
        if (Console::get_input("Are you sure to overwrite it? (y or n) (default y):", "y") === "n") Console::end();
    }

    $req_values = [
        "SYSTEM_KEY" => ["This key is the password which you will use from the server", "1234"]
    ];

    foreach ($req_values as $key => $value) {
        Console::print($value[0]);
        $req_values[$key] = Console::get_input("Value for {$key} (default {$value[1]}): ", $value[1]);
        Console::print();
    }

    if (file_put_contents(".env", json_encode($req_values, JSON_PRETTY_PRINT)) !== false) {
        Console::success("Written the configuration file.");
    }
    else {
        Console::error("Error in writing file. Try manually");
        Console::print("\n" . json_encode($req_values, JSON_PRETTY_PRINT) . "\n");
    }
} else {
    Console::fatal($usage);
}

Console::end();

