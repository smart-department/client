<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/class/autoloader.php");

$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";

if ($method === "GET") {
    $KEYS = new Keys();
    
    if (!$KEYS->validate_auth(Header::get_token())) {
        Response::send(null, 401);
    }
    
    $memory = new Memory();
    $cpu = new Cpu();
    $system = new System($memory, $cpu);
    
    Response::send($system->get_overview());

} else if ($method === "PATCH") {

    $request = Request::parse();
    $KEYS = new Keys();
    
    if (!$KEYS->validate_auth(Header::get_token())) {
        Response::send(null, 401);
    }

    if (empty((array) $request)) {
        Response::send(null, 400);
    }

    $available_for_edit = [
        ["SYSTEM_KEY", "key"]
    ];
    
    try {
        $current_env = $data = json_decode(file_get_contents(__DIR__ ."/.env"), false, 512, JSON_THROW_ON_ERROR);
        $updated_value = [];

        foreach ($available_for_edit as $option) {  
            if (!empty($request->{ $option[1] })) {
                $current_env->{ $option[0] } = $request->{ $option[1] };
                $updated_value[$option[1]] = $request->{ $option[1] };
            }
        }
        if (file_put_contents(__DIR__ ."/.env", json_encode($current_env, JSON_PRETTY_PRINT)) === false) {
            throw "Couldn't write to file";
        }

        Response::send($updated_value, 200, "Updated information");
    } catch (Exception $e) {
        Response::send(null, 500, "Error while updating the key");
    }
    
} else {
    Response::not_found();
}